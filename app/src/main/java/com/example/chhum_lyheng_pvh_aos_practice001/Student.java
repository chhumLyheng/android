package com.example.chhum_lyheng_pvh_aos_practice001;

import java.io.Serializable;

public class Student implements Serializable {
    String name;
    int age;
    String university;

    public Student(String name, int age, String university) {
        this.name = name;
        this.age = age;
        this.university = university;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String isUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }
}
