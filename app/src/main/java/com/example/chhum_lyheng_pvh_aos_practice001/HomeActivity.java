package com.example.chhum_lyheng_pvh_aos_practice001;

import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        textView = findViewById(R.id.textView);
        Intent valueIntent = getIntent();

        String value = valueIntent.getStringExtra("value");


        textView.setText(value);

        valueIntent.putExtra("close", "Close App ");
        textView.setOnClickListener(View -> {
            setResult(RESULT_OK, valueIntent);
            finish();

        });


    }
}